//加载设备信息
function initDeviceInfo() {
    //设备信息
    var deviceInfo = [{id: "1", type: "sxj", name: "摄像机01", nodeTag: 8941}
        , {id: "2", type: "sxj", name: "摄像机02", nodeTag: 3236}
        , {id: "3", type: "sxj", name: "摄像机03", nodeTag: "camera_2_1"}
        , {id: "4", type: "sxj", name: "摄像机04", nodeTag: 8934}];
    initCanse(deviceInfo);
}
;
//初始化设备
function initCanse(deviceInfo) {
    //添加双击事件
    g3d.mi(function (e) {
        if (e.kind === 'doubleClickData') {
            if (e.data !== undefined) {
                //摄像机设备
                if (e.data.getAttr('deviceType') === "sxj") {
                    var deviceId = e.data.getAttr('deviceId');
                    var videoT = document.getElementById("video_" + deviceId);
                    var display = videoT.style.display;
                    if (display === "block") {
                        //双击关闭面板
                        videoT.style.display = "none";
                        //关闭视频流
                        document.getElementById("videoMp4_" + deviceId).innerHTML = "";
                    } else {
                        //双击显示面板
                        videoT.style.left = "0px";
                        videoT.style.top = "0px";
                        videoT.style.display = "block";
                        //播放视频
                        initVideo(deviceId);
                    }

                }
            }
        }
    });
    //添加设备面板
    for (var i = 0; i < deviceInfo.length; i++) {
        var deviceId = "";
        if (deviceInfo[i].id !== null && deviceInfo[i].id !== "" && deviceInfo[i].id !== undefined) {
            deviceId = deviceInfo[i].id;
        }
        var deviceType = deviceInfo[i].type;
        if (deviceInfo[i].type !== null && deviceInfo[i].type !== "" && deviceInfo[i].type !== undefined) {
            deviceType = deviceInfo[i].type;
        }
        var deviceName = deviceInfo[i].name;
        if (deviceInfo[i].name !== null && deviceInfo[i].name !== "" && deviceInfo[i].name !== undefined) {
            deviceName = deviceInfo[i].name;
        }
        var nodeTag = "";
        if (deviceInfo[i].nodeTag !== null && deviceInfo[i].nodeTag !== "" && deviceInfo[i].nodeTag !== undefined) {
            nodeTag = deviceInfo[i].nodeTag;
        }
        //获取模型中的摄像机
        var node2 = dm3d.getDataByTag(nodeTag);
        if (node2 !== undefined) {
            node2.setName("video");
            node2.s('label', deviceName);
            node2.setAttr('deviceType', deviceType);
            node2.setAttr('deviceId', deviceId);
            node2.s('label.visible', "false");
            node2.s('label.color', "#000000");
            node2.s('label.font', 'bold 24px arial');
            node2.s('label.position', "3");
            var videodiv = document.createElement('div');
            var tempDiv = "<ul><li><label for='startPicker_"+deviceId+"'>开始时间：</label><input id='startPicker_"+deviceId+"' style='width: 185px'  />";
            tempDiv += "<label for='endedPicker_"+deviceId+"'>结束时间：</label><input id='endedPicker_"+deviceId+"' style='width: 185px' /></li>";
            tempDiv += "<li><input id='palyBackBtn_"+deviceId+"' type='button' value='视频回放' class='k-button'style='width: 185px' >";
            tempDiv += "<input id='palyRealBtn_"+deviceId+"' type='button' value='实时视频' class='k-button' style='width: 185px;margin-left:10px'></li></ul>";
            var html = "<div id = 'video_" + deviceId + "' style='width:600px;height:500px;display: none;position: absolute;z-index: 4;border: 2px solid #00AFF5;background-color: #000000;' onmouseup='dragEnd(event,this)' onmousemove='drag(event,this)' onmousedown='dragStart(event,this)' >" +
                    "<div  style='width:600px;height:50px;display:block;z-index: 4;background-color: #00AFF5;'>" +
                    "<div style='position:relative;display: block;float: left;width: 300px;height: 30px;margin: 10px 0 0 20px;font-size: 20px;text-align: left;line-height: 25px;color: #ffffff;'>" + deviceName + "</div>" +
                    "<div  style='position:relative;display: block;float: left;width: 30px;height: 30px;margin: 10px 0 0 240px;cursor: pointer;border: 1px solid #CACACA;font-size: 20px;text-align: center;line-height: 25px;color: #ffffff;'  onclick='closeVideo(" + deviceId + ")'>x</div>" +
                    "</div>" +
                    "<div>"+
                    tempDiv+
                    "</div>" +
                    "<div id='videopanel_" + deviceId + "' style='width:600px;height:450px;display:block;z-index: 4;'>" +
                    "<video id ='videoMp4_" + deviceId + "'  width='600px'  height='450px' autoplay='true' ishivideo='true' isrotate='true' autoHide='true' controls='controls'>" +
                    "</video>" +
                    "</div>" +
                    "</div>";
            $("body").append(html);
        }
    }
}
//播放视频
function initVideo(deviceId) {
    document.getElementById("videoMp4_" + deviceId).innerHTML = "<source id ='videoMp4_" + deviceId + "' src='./video/video_" + deviceId + ".mp4' type='video/mp4' width='600px'  height='450px'/>";
}
//关闭视频
function closeVideo(v) {
    var videoT = document.getElementById("video_" + v);
    document.getElementById("videoMp4_" + v).innerHTML = "";
    videoT.style.display = "none";
}
//拖拽
function drag(e, v) {
    var dragable = v.dragable;
    var dragstartE = v.dragstartE;
    var startX = v.startX;
    var startY = v.startY;
    if (dragstartE === undefined) {
        dragstartE = e;
    }
    if (startX === undefined) {
        startX = 0;
    }
    if (startY === undefined) {
        startY = 0;
    }
    var y = e.pageY - dragstartE.pageY;
    var x = e.pageX - dragstartE.pageX;
    if (dragable) {
        v.style.left = (startX + x) + "px";
        v.style.top = (startY + y) + "px";
    }
}
//开始拖拽
function dragStart(e, v) {
    v.startX = v.offsetLeft;
    v.startY = v.offsetTop;
    v.dragable = true;
    v.dragstartE = e;
}
//结束拖拽
function dragEnd(e, v) {
    v.dragable = false;
}